<h4>Программа выполнена по варианту 10 учеником группы КИ21-17/2Б Ершовым Тимофеем на языке Python.</h4>
<h5>Требуется написать две функции:</h5>
<div>- Первая будет считать количество ходов, необходимое коню чтобы
добраться из одной клетки шахматной доски до другой;</div>
<div>- Вторая будет считать, через какое минимальное количество ходов могут встретиться два коня, расположенных на двух разных клетках доски.</div>
<h5>Программа реализует три функции:</h5>
<div>- Функция main, в которой происходит ввод пользователем значений и вывод результатов выичслений на экран</div>
<div>- Функция knight_move, которая принимает на вход два кортежа (каждый из которых содержит по два числа) и выдаёт результат вычислений</div>
<div>- Функция knight_collision, которая представляет из себя полную копию knight_move, за исключением того, что теперь конь должен находится рядом с обозначенной точкой, а не в ней</div>
<div>Чтобы избежать ошибок при обработке неправильно введённых значений, была реализована проверка ввода в функции main:</div>
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;"><pre style="margin: 0; line-height: 125%">        <span style="color: #008800; font-weight: bold">try</span>:
            start_position <span style="color: #333333">=</span> <span style="color: #007020">input</span>(<span style="background-color: #fff0f0">&quot;Enter the first &quot;</span>
                                   <span style="background-color: #fff0f0">&quot;coordinates (separated by a space):&quot;</span>)<span style="color: #333333">.</span>split()
            finish_position <span style="color: #333333">=</span> <span style="color: #007020">input</span>(<span style="background-color: #fff0f0">&quot;Enter the second &quot;</span>
                                    <span style="background-color: #fff0f0">&quot;coordinates (separated by a space):&quot;</span>)<span style="color: #333333">.</span>split()
            <span style="color: #008800; font-weight: bold">for</span> position <span style="color: #000000; font-weight: bold">in</span> <span style="color: #007020">range</span>(<span style="color: #0000DD; font-weight: bold">2</span>):
                start_position[position] <span style="color: #333333">=</span> <span style="color: #007020">int</span>(start_position[position])
            <span style="color: #008800; font-weight: bold">for</span> position <span style="color: #000000; font-weight: bold">in</span> <span style="color: #007020">range</span>(<span style="color: #0000DD; font-weight: bold">2</span>):
                finish_position[position] <span style="color: #333333">=</span> <span style="color: #007020">int</span>(finish_position[position])
        <span style="color: #008800; font-weight: bold">except</span>:
            <span style="color: #008800; font-weight: bold">del</span> start_position
            <span style="color: #008800; font-weight: bold">print</span>(<span style="background-color: #fff0f0">&quot;Wrong input&quot;</span>)
            <span style="color: #008800; font-weight: bold">continue</span>
        <span style="color: #008800; font-weight: bold">if</span> <span style="color: #007020">len</span>(start_position) <span style="color: #333333">&gt;</span> <span style="color: #0000DD; font-weight: bold">2</span> <span style="color: #000000; font-weight: bold">or</span> <span style="color: #007020">len</span>(finish_position) <span style="color: #333333">&gt;</span> <span style="color: #0000DD; font-weight: bold">2</span>:
            <span style="color: #008800; font-weight: bold">print</span>(<span style="background-color: #fff0f0">&quot;Wrong input&quot;</span>)
            <span style="color: #008800; font-weight: bold">continue</span>
</pre></div>
<div>Результат работы программы будет следующий:</div>
<img src="https://gitlab.com/practical_works1/practical6/-/raw/main/изображение_2021-12-29_165027.png" alt="Результат выполнения программы"/>
<div>Ресурсы, использованные во время выполнения практической работы:</div>
<div><a href="https://lavelin.ru/wp-content/uploads/2018/11/dev-blogs.pdf">Как залить локальный репозиторий на удалённый</a></div>
<div><a href="gitlab.com/">Удалённый репозиторий</a></div>
<div><a href="http://htmlbook.ru/html">Справочник HTML</a></div>
