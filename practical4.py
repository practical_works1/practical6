def knight_move(start, end) -> int:
    """
    The function searches for moves that the knight can make while at
    a certain point, and then finds the one that is the most preferred.
    The process continues until the knight arrives at the designated point
    :param start:Coordinates from where the knight starts its movement
    :param end:Coordinates where the knight should get to
    """
    count = 0
    status = "is_working"
    moving = {start: 0}

    def possile_moving(position):
        board = [1, 2, 3, 4, 5, 6, 7, 8]
        temporary_moves = []
        for x_position in [-1, 1]:
            for y_position in [-2, 2]:
                if (position[0] + x_position) in board \
                        and (position[1] + y_position) in board:
                    temporary_moves.append((position[0]
                                            + x_position, position[1] + y_position))
                elif (position[1] + x_position) \
                        in board and (position[0] + y_position):
                    temporary_moves.append((position[1]
                                            + x_position, position[0] + y_position))
        return temporary_moves
    while status == "is_working":
        temporary_moves = {}
        count += 1
        if end in moving:
            return count - 1
        for x_position in moving:
            for y_position in possile_moving(x_position):
                if y_position not in moving:
                    temporary_moves[y_position] = count
        moving.update(temporary_moves)


def knight_collision(start, end) -> int:
    """
    The function does the same thing as knight_move with one
    change - the knight must come to any of the four points
    that are around the designated
    :param start:Coordinates from where the knight starts its movement
    :param end:Coordinates where the knight should get to
    """
    count = 0
    status = "is_working"
    moving = {start: 0}
    if start == end:
        return 0
    def possile_moving(position):
        board = [1, 2, 3, 4, 5, 6, 7, 8]
        temporary_moves = []
        for x_position in [-1, 1]:
            for y_position in [-2, 2]:
                if (position[0] + x_position) in board \
                        and (position[1] + y_position) in board:
                    temporary_moves.append((position[0]
                                            + x_position, position[1] + y_position))
                elif (position[1] + x_position) \
                        in board and (position[0] + y_position):
                    temporary_moves.append((position[1]
                                            + x_position, position[0] + y_position))
        return temporary_moves
    end_variations = [(end[0], end[1]-1),
                      (end[0]-1, end[1]),
                      (end[0]+1, end[1]),
                      (end[0], end[1]+1)]
    while status == "is_working":
        temporary_moves = {}
        count += 1
        for variations in end_variations:
            if variations in moving:
                return count - 1
        for x_position in moving:
            for y_position in possile_moving(x_position):
                if y_position not in moving:
                    temporary_moves[y_position] = count
        moving.update(temporary_moves)


def main():
    """
    A function that implements user input of
    values and output of calculation results
    """
    status = "is_working"
    while status == "is_working":
        try:
            start_position = input("Enter the first "
                                   "coordinates (separated by a space):").split()
            finish_position = input("Enter the second "
                                    "coordinates (separated by a space):").split()
            for position in range(2):
                start_position[position] = int(start_position[position])
            for position in range(2):
                finish_position[position] = int(finish_position[position])
        except:
            del start_position
            print("Wrong input")
            continue
        if len(start_position) > 2 or len(finish_position) > 2:
            print("Wrong input")
            continue
        else:
            start_position = tuple(start_position)
            finish_position = tuple(finish_position)
            print("Number of steps:", knight_move(start_position, finish_position))
            print("The number of steps for the knights to come to each other:",
                  knight_collision(start_position, finish_position))
            decision = input("Do you want to restart the program? "
                             "(yes to continue, any other character to exit):")
            if decision == "yes":
                continue
            status = "is_done"


if __name__ == '__main__':
    main()
